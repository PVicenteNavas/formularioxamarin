﻿using System;
using System.Diagnostics;
using System.Windows.Input;
using Xamarin.Forms;
using XamarinNotificaciones.Models;
using XamarinNotificaciones.Services;
using Xamarin.Essentials;

namespace XamarinNotificaciones.ViewModels
{
    public class MainViewModel
    {
        public ICommand LoginCommand => new Command(LoginComprobar);

        public string LoginEntry { get; set; }
        public string PasswordEntry { get; set; }

        public bool UserBool = false;
        public bool PassBool = false;

        public void LoginComprobar()
        {
            User user = new User();

            user.Login = "Pedro";
            user.Password = "hola";

            if (LoginEntry.Equals(user.Login, StringComparison.InvariantCultureIgnoreCase))
            {
                Debug.WriteLine("El Usuario es correcto");
                UserBool = true;
            }
            else
            {
                Debug.WriteLine("El Usuario es incorrecto");
                UserBool = false;
            }

            if (PasswordEntry.Equals(user.Password, StringComparison.InvariantCultureIgnoreCase))
            {
                Debug.WriteLine("La Contraseña es correcta");
                PassBool = true;
            }
            else
            {
                Debug.WriteLine("La Contraseña es incorrecta");
                PassBool = false;
            }

            if (PassBool && UserBool)
            {
                Application.Current.MainPage.DisplayAlert("Acceso", "Login correcto", "Noice");

                //Detecta el primer arranque de la aplicacion
                if (VersionTracking.IsFirstLaunchEver)
                {
                    Application.Current.MainPage.DisplayAlert("Primer inicio", "Este es el primer inicio de todos",
                        "VALE");
                }

                //Para saber que tipo de dispositivo es
                if (Device.RuntimePlatform == Device.iOS)
                {
                    DependencyService.Get<INotification>()
                        .CreateNotification("Login Exitoso", "Registrado con Firebase (Local) Dispositivo IOS");
                }
                else if (Device.RuntimePlatform == Device.Android)
                {
                    DependencyService.Get<INotification>()
                        .CreateNotification("Login Exitoso", "Registrado con Firebase (Local) Dispositivo Android");
                }
            }
            else
            {
                Application.Current.MainPage.DisplayAlert("Acceso", "Login incorrecto", "No noice");
            }
        }
    }
}