﻿namespace XamarinNotificaciones.Services
{
    public interface INotification
    {
        void CreateNotification(string title, string message);
    }
}